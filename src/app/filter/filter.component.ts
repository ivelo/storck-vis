import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {StorckService} from "../core/storck.service";
import {Store} from "../core/store.service";
import {Actions} from "../core/actions.service";
import { FormControl } from "@angular/forms";

@Component({
  selector: 'filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  filename: string = '';
  dateFrom!: Date | null;
  dateTo!: Date | null;
  metadata: { key: string, value: string, comparisonSelect: FormControl, active: boolean }[] = [];

  newKey: string = '';
  newValue: string = '';
  comparisonSelect = new FormControl('iexact')

  comparisons = [
    { name: 'Equal', value: 'iexact' },
    { name: 'Contains', value: 'icontains' },
    { name: 'Greater than', value: 'gt' },
    { name: 'Lesser than', value: 'lt' },
  ];

  updateFilesTimeout: any;

  @ViewChild('newKeyInput')
  newKeyInput!: ElementRef<HTMLInputElement>;

  constructor(private storckService: StorckService, private store: Store, private actions: Actions) { }

  ngOnInit(): void {
    this.store.changeTabEvent.subscribe(tab => {
      if (tab === 'Filters') {
        this.updateFiles();
      }
    });
  }

  addMetadata(event: any) {
    if (
      ((!event || !event.relatedTarget) ||
      event.relatedTarget.getAttribute('name') !== 'newMetadataKey'
      && event.relatedTarget.getAttribute('name') !== 'newMetadataValue'
      && event.relatedTarget.getAttribute('name') !== 'newMetadataComparison'
      && !this.comparisons.map(e => e.value).includes(event.relatedTarget.getAttribute('ng-reflect-value'))
      )
      && this.newKey
      && this.newValue)
    {
      const data = this.metadata.find(element => element.key === this.newKey)
      if (data) {
        data.value = this.newValue;
        data.active = true;
      }
      else {
        this.metadata.push({
            key: this.newKey,
            value: this.newValue,
            comparisonSelect: new FormControl(this.comparisonSelect.value),
            active: true,
          });
      }
      this.newKey = '';
      this.newValue = '';
      this.comparisonSelect.setValue('iexact');
      this.newKeyInput.nativeElement.focus();
      this.updateFiles();
    }
  }

  deleteMetadata(key: string) {
    this.metadata = this.metadata.filter(data => data.key !== key);
    this.updateFiles();
  }

  filterChange() {
    if (this.updateFilesTimeout) {
      clearTimeout(this.updateFilesTimeout);
    }
    this.updateFilesTimeout = setTimeout(() => {
      this.updateFiles();
    }, 800);

  }

  updateFiles() {
    if (this.store.workspace) {
      const dateFrom = this.parseDate(this.dateFrom);
      const dateTo = this.parseDate(this.dateTo, 1);
      const metadata: any = {};
      this.metadata.filter(data => data.active).forEach(data => {
        let value: string | number = data.value;
        if (data.comparisonSelect.value !== 'icontains' && !isNaN(parseFloat(value))) {
          value = parseFloat(value);
        }
        metadata['metadata__' + data.key + '__' + data.comparisonSelect.value] = value;
      });
      const meta_search = {
        stored_path__contains: this.filename || undefined,
        ...metadata,
        date__gte: dateFrom || undefined,
        date__lte: dateTo || undefined,
      };
      if (Object.values(meta_search).filter(value => value).length) {
        this.storckService.getFiles(this.store.workspace.tokens[0].token, meta_search)
          .subscribe(files => {
            this.actions.changeViewedFiles('', files, false);
          });
      }
      else {
        this.actions.changeViewedFiles('', [], false);
      }
    }
  }

  parseDate(date: Date | null, addDays: number = 0): string | null {
    return date ? date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + (date.getDate() + addDays) : null;
  }

}
