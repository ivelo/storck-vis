import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DirectoriesListComponent} from "./directories-list/directories-list.component";
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http'
import {StorckInterceptor} from "./core/storck-interceptor";
import {AutoFocusDirective} from "./utils/autofocus.directive";
import { FooterComponent } from './footer/footer.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { FilesListComponent } from './files-list/files-list.component';
import { FileViewComponent } from './files-list/file-view/file-view.component';
import { ActionPanelComponent } from './action-panel/action-panel.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {WorkspacesComponent} from "./workspaces/workspaces.component";
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatExpansionModule} from '@angular/material/expansion';
import {FilterComponent} from "./filter/filter.component";
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatTabsModule} from '@angular/material/tabs';
import {MatBadgeModule} from '@angular/material/badge';
import {MatSelectModule} from '@angular/material/select';
import {MatChipsModule} from '@angular/material/chips';
import { AuthModule } from './auth/auth.module';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import {MetadataRulesListComponent} from "./metadata-rules-list/metadata-rules-list.component";
import {NgJsonEditorModule} from "ang-jsoneditor";
import {MatDialogModule} from "@angular/material/dialog";
import {CredentialsDialogComponent} from "./action-panel/credentials-dialog/credentials-dialog.component";
import {AddPersonDialogComponent} from "./workspaces/add-person-dialog/add-person-dialog.component";
import {MatListModule} from "@angular/material/list";
import {MatMenuModule} from "@angular/material/menu";


@NgModule({
  declarations: [
    AppComponent,
    DirectoriesListComponent,
    WorkspacesComponent,
    AddPersonDialogComponent,
    AutoFocusDirective,
    FooterComponent,
    ToolbarComponent,
    FilesListComponent,
    FileViewComponent,
    ActionPanelComponent,
    CredentialsDialogComponent,
    FilterComponent,
    MetadataRulesListComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatCardModule,
    MatExpansionModule,
    MatInputModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatTabsModule,
    MatBadgeModule,
    MatSelectModule,
    MatChipsModule,
    AuthModule,
    AppRoutingModule,
    NgJsonEditorModule,
    MatDialogModule,
    MatListModule,
    MatMenuModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: StorckInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
