import {Injectable} from "@angular/core";
import {KeycloakOptions, KeycloakService} from "keycloak-angular";
import {KeycloakLoginOptions} from "keycloak-js";
import {StorckService} from "../../core/storck.service";
import {Actions} from "../../core/actions.service";
import {lastValueFrom, map} from "rxjs";
import {getCookie} from "../../utils/cookies";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: 'root'
})
export class StorckKeycloakService extends KeycloakService {
  constructor(private storckService: StorckService, private actions: Actions) {
    super();
  }

  override init(options?: KeycloakOptions | undefined) {
    return super.init(options).then(res => {
      this.actions.changeUserToken(this.getKeycloakInstance().token || '');
      const cookieToken = getCookie('token');
      if (cookieToken) {
        this.actions.changeUserToken(cookieToken);
      }
      else if (this.getKeycloakInstance().authenticated) {
        const data = new URLSearchParams();
        data.append('client_id', environment.clientId);
        data.append('audience', environment.storckClientId);
        data.append('grant_type', 'urn:ietf:params:oauth:grant-type:token-exchange');
        data.append('requested_token_type', 'urn:ietf:params:oauth:token-type:refresh_token');
        data.append('subject_token', this.getKeycloakInstance().token || '');
        return fetch(environment.tokenExchangeUrl, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          body: data,
        }).then(exchangeResponse => {
          return exchangeResponse.json().then(exchangeJson => {
            this.actions.changeUserToken(exchangeJson.access_token);
            return lastValueFrom(this.storckService.authenticate().pipe(map(storckVisToken => {
              this.actions.changeUserToken(storckVisToken);
              document.cookie = 'token=' + storckVisToken;
              return res;
            })));
          })

        });
      }
      return res;
    });
  }

  override logout(redirectUri?: string | undefined) {
    document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
    return super.logout(redirectUri);
  }
}
