// Load node modules
const colors = require('colors');
const fs = require('fs');
const dotenv = require('dotenv');
dotenv.config();

// Configure Angular `environment.ts` file path
const targetPath = './src/environments/environment.prod.ts';

// Read initial json config
let envConfig = fs.readFileSync('./src/environments/env-config.json');
envConfig = JSON.parse(envConfig);
console.log(colors.magenta('Initial config file that will be overwritten:'))
console.log(envConfig);

// Replace values with environment variables
const storckClientId = process.env['STORCK_CLIENT_ID'];
if (storckClientId !== undefined) {
    envConfig['storckClientId'] = storckClientId;
}

const apiUrl = process.env['API_URL'];
if (apiUrl !== undefined) {
    envConfig['apiUrl'] = apiUrl;   
}

const clientId = process.env['CLIENT_ID'];
if (clientId !== undefined) {
    envConfig['clientId'] = clientId;   
}

// Generate `environment.prod.ts`
const envConfigFile = `export const environment: any = {
    production: '${envConfig['production']}',
    storckClientId: '${envConfig['storckClientId']}',
    clientId: '${envConfig['clientId']}',
    realm: '${envConfig['realm']}',
    authUrl: '${envConfig['authUrl']}',
    tokenExchangeUrl: '${envConfig['tokenExchangeUrl']}',
    apiUrl: '${envConfig['apiUrl']}'
};`;
console.log(colors.magenta('\nThe file `environment.prod.ts` will be written with the following content:'));
console.log(colors.grey(envConfigFile));

fs.writeFile(targetPath, envConfigFile, function (err) {
   if (err) {
       throw console.error(err);
   } else {
       console.log(colors.magenta(`\nAngular environment.prod.ts file generated correctly at ${targetPath} \n`));
   }
});