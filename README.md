Project was created by Maciej Majewski.

# Run locally
Install dependencies:
```bash
npm install
```
Run application on 'http://localhost:4200/' by typing:
```bash
ng serve
```
# Run in docker container
To build new image you need to add new tag in repository.


Run container:
```bash
docker compose up -d 
```
Stop container:
```bash
docker compose stop
```

# Changing configuration
To change configuration values, set environment variables: STORCK_CLIENT_ID, CLIENT_ID, API_URL.
